# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="2.2.4"></a>
## [2.2.4](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.2.3...v2.2.4) (2018-04-24)



<a name="2.2.3"></a>
## [2.2.3](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.2.2...v2.2.3) (2018-04-24)



<a name="2.2.2"></a>
## [2.2.2](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.2.1...v2.2.2) (2018-04-24)



<a name="2.2.1"></a>
## [2.2.1](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.2.0...v2.2.1) (2018-04-24)



<a name="2.2.0"></a>
# [2.2.0](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.0.0...v2.2.0) (2018-04-24)


### Features

* **skip ci message:** fpgfwpgfwpg ([f98c5e6](https://gitlab.com/joefraley/perfect-workflow-demo/commit/f98c5e6))



<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v2.0.0...v2.1.0) (2018-04-24)


### Features

* **skip ci message:** fpgfwpgfwpg ([f98c5e6](https://gitlab.com/joefraley/perfect-workflow-demo/commit/f98c5e6))



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v1.1.1...v2.0.0) (2018-04-24)


### Bug Fixes

* **arstart:** wfpwfp ([0282d53](https://gitlab.com/joefraley/perfect-workflow-demo/commit/0282d53))
* **tada a bug:** its fixed ([eef8ffd](https://gitlab.com/joefraley/perfect-workflow-demo/commit/eef8ffd))


### Features

* **arstarst:** arstarst ([d89853a](https://gitlab.com/joefraley/perfect-workflow-demo/commit/d89853a))
* **arstarst:** fwqpqwfpqw ([d6415c3](https://gitlab.com/joefraley/perfect-workflow-demo/commit/d6415c3))
* **arstarst:** wfpwfp ([0b2e606](https://gitlab.com/joefraley/perfect-workflow-demo/commit/0b2e606))
* **arstarts:** wqpqwfp ([b49e0b6](https://gitlab.com/joefraley/perfect-workflow-demo/commit/b49e0b6))
* **deployment:** paramaterize now deploys ([7fba445](https://gitlab.com/joefraley/perfect-workflow-demo/commit/7fba445))
* **deps:** a whole new world 2 ([4b92a0d](https://gitlab.com/joefraley/perfect-workflow-demo/commit/4b92a0d))


### Tests

* **arstasrt:** asrtarst ([86d65f7](https://gitlab.com/joefraley/perfect-workflow-demo/commit/86d65f7))


### BREAKING CHANGES

* **arstasrt:** arstarst



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v1.1.0...v1.1.1) (2018-04-23)


### Bug Fixes

* npm vers ([a534336](https://gitlab.com/joefraley/perfect-workflow-demo/commit/a534336))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/joefraley/perfect-workflow-demo/compare/v1.0.0...v1.1.0) (2018-04-23)


### Bug Fixes

* new thing ([044781b](https://gitlab.com/joefraley/perfect-workflow-demo/commit/044781b))
* whatever ([4904452](https://gitlab.com/joefraley/perfect-workflow-demo/commit/4904452))


### Features

* my new thing ([3f7ffae](https://gitlab.com/joefraley/perfect-workflow-demo/commit/3f7ffae))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/joefraley/perfect-workflow-demo/compare/37a94a7...v1.0.0) (2018-04-23)


### Bug Fixes

* a whole new world! ([86db766](https://gitlab.com/joefraley/perfect-workflow-demo/commit/86db766))
* see the token duh ([4d0d547](https://gitlab.com/joefraley/perfect-workflow-demo/commit/4d0d547))
* tada! ([7c1100a](https://gitlab.com/joefraley/perfect-workflow-demo/commit/7c1100a))


### Features

* a new message! ([f35a2bd](https://gitlab.com/joefraley/perfect-workflow-demo/commit/f35a2bd))
* new stuff! ([37a94a7](https://gitlab.com/joefraley/perfect-workflow-demo/commit/37a94a7))
* some new config ([7365bc7](https://gitlab.com/joefraley/perfect-workflow-demo/commit/7365bc7))
* the yaml is better ([591f24d](https://gitlab.com/joefraley/perfect-workflow-demo/commit/591f24d))
