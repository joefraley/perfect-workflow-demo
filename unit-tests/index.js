// https://github.com/substack/tape
// https://github.com/leebyron/testcheck-js/tree/4e3240bda2dc9aa0cb251ecd42d3baf5ec12513c/integrations/tape-check
//
const { demo } = require("../src/demo");
const { check, gen } = require("tape-check");
const test = require("tape");

test(
  "boolean logic",
  check({ times: 100 }, gen.boolean, (t, condition) => {
    t.plan(1);
    t.equal(condition && demo(condition), demo(condition) && condition);
  }),
);
